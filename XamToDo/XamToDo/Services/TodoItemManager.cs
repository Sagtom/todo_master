﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using XamToDo.Model;

namespace XamToDo.Services
{
    public class TodoItemManager
    {
        IRestService restService;

        public TodoItemManager(IRestService service)
        {
            restService = service;
        }

        public Task<ObservableCollection<ToDoTask>> GetTasksAsync()
        {
            return restService.RefreshDataAsync();
        }

        public Task SaveTaskAsync(ToDoTask item, bool isNewItem = false)
        {
            return restService.SaveToDoTaskAsync(item, isNewItem);
        }

        public Task DeleteTaskAsync(ToDoTask item)
        {
            return restService.DeleteToDoTaskAsync(item._id);
        }
    }
}
