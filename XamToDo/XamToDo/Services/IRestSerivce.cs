﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using XamToDo.Model;

namespace XamToDo
{
    public interface IRestSerivce
    {
        Task<List<ToDoTask>> RefreshDataAsync();
        Task SaveTodoItemAsync(ToDoTask item, bool isNewItem);
        Task DeleteTodoItemAsync(string id);
    }
}
