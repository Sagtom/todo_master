﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using XamToDo.Model;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Collections.ObjectModel;

namespace XamToDo.Services
{
    class RestService : IRestService
    {
        HttpClient client;

        public ObservableCollection<ToDoTask> Items { get; private set; }

        public RestService()
        {
            //var authData = string.Format("{0}:{1}", ConstantValue.Username, ConstantValue.Password);
            //var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public async Task<ObservableCollection<ToDoTask>> RefreshDataAsync()
        {
            Items = new ObservableCollection<ToDoTask>();

            var uri = new Uri(string.Format(ConstantValue.RestUrl, string.Empty));

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Items = JsonConvert.DeserializeObject<ObservableCollection<ToDoTask>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return Items;
        }

        public async Task SaveToDoTaskAsync(ToDoTask item, bool isNewItem = false)
        {
            var uri = new Uri(string.Format(ConstantValue.RestUrl, string.Empty));

            try
            {
                var json = JsonConvert.SerializeObject(item);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                
                HttpResponseMessage response = null;
                if (isNewItem)
                {
                    response = await client.PostAsync(uri, content);
                }
                else
                {
                    response = await client.PutAsync(uri, content);
                }

                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"				ToDoTask successfully saved.");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }

      
        public async Task DeleteToDoTaskAsync(string id)
        {
            string uri = ConstantValue.RestUrl + id;
            try
            {
                var response = await client.DeleteAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    Debug.WriteLine(@"				ToDoTask successfully deleted.");
                }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
        }

        public Task SaveTodoItemAsync(ToDoTask item, bool isNewItem)
        {
            throw new NotImplementedException();
        }

        public Task DeleteTodoItemAsync(string id)
        {
            throw new NotImplementedException();
        }
    }
}
