﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamToDo.Model;

namespace XamToDo.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TodoListPage : ContentPage
	{
        bool alertShown = false;

        public TodoListPage()
        {
            InitializeComponent();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            if (ConstantValue.RestUrl.Contains("localhost"))
            {
                if (!alertShown)
                {
                    await DisplayAlert(
                        "Hosted Back-End",
                        "My REST Service",
                        "OK");
                    alertShown = true;
                }
            }

            listView.ItemsSource = await App.TodoManager.GetTasksAsync();
        }

        void OnAddItemClicked(object sender, EventArgs e)
        {
            var todoItem = new ToDoTask()
            {
                _id = Guid.NewGuid().ToString()
            };
            var todoPage = new TodoItemPage(true);
            todoPage.BindingContext = todoItem;
            Navigation.PushAsync(todoPage);
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var todoItem = e.SelectedItem as ToDoTask;
            var todoPage = new TodoItemPage();
            todoPage.BindingContext = todoItem;
            Navigation.PushAsync(todoPage);
        }
    }
}