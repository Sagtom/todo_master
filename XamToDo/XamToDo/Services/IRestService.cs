﻿using System;
using System.Collections.Generic;
using System.Text;
using XamToDo.Model;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace XamToDo.Services
{
    public interface IRestService
    {
        Task<ObservableCollection<ToDoTask>> RefreshDataAsync();
        Task SaveToDoTaskAsync(ToDoTask item, bool isNewItem);
        Task DeleteToDoTaskAsync(string id);
    }
}
