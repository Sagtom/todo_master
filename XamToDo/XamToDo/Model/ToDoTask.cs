﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamToDo.Model
{
    public class ToDoTask
    {
        public string _id { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public bool Done { get; set; }
    }
}
